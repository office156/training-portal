import React from 'react'
import Navbar from '../../Components/Navbar/Navbar'
import Sidebar from '../../Components/Sidebar/Sidebar'
import "./AdminPanel.css"

export default function AdminPanel() {
  return (
    <div className="adminPanelTop">
      <Sidebar className = "adminPanelSidebartag"></Sidebar>


<div className="contentAdminPanel">
  <div className="container">
    <div className="row py-5 d-flex justify-content-around">
      <div className="col-3 adminPanelHomeStatusCards">
        <div className="line1 d-flex justify-content-between pt-3 px-1">
          <p className="line1text1">
          Total Courses
          </p>
          <i class="fa-solid fa-ellipsis-vertical"></i>
        </div>
        <div className="line2 d-flex justify-content-between align-items-end pb-3 px-1">
        <p className="line1text2">
          12
          </p>
          <a className='viewAllLink'>View all</a>
        </div>
      </div>
      <div className="col-3 adminPanelHomeStatusCards">
        <div className="line1 d-flex justify-content-between pt-3 px-1">
          <p className="line1text1">
          Total Active Users
          </p>
          <i class="fa-solid fa-ellipsis-vertical"></i>
        </div>
        <div className="line2 d-flex justify-content-between align-items-end pb-3 px-1">
        <p className="line1text2">
          230
          </p>
          <a className='viewAllLink'>View all</a>
        </div>
      </div>
      <div className="col-3 adminPanelHomeStatusCards">
        <div className="line1 d-flex justify-content-between pt-3 px-1">
          <p className="line1text1">
          Total Inactive Users
          </p>
          <i class="fa-solid fa-ellipsis-vertical"></i>
        </div>
        <div className="line2 d-flex justify-content-between align-items-end pb-3 px-1">
        <p className="line1text2">
          56
          </p>
          <a className='viewAllLink'>View all</a>
        </div>
      </div>
    </div>
  </div>
</div>

    </div>
  )
}
