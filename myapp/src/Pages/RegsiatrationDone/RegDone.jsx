import React from 'react'
import "./RegDone.css"

export default function RegDone
    () {
    return (
        <div className='regDoneTop text-center container d-flex justify-content-center align-items-center'>
            <p className='p-5 border paragraphDesign'>  Dear user , <br /> Thank you for registring with us. Currently your account is
                inactive. You will be able to login after Photo and Aadhar card verification.
                <br /> You will get an confirmation email on your registered email id after successful
                verification.
            </p>
        </div>

    )
}
