import React from 'react'
import "./Login.css"
import { useNavigate } from 'react-router-dom';

export default function Login() {
  
  const navigate = useNavigate();

  return (
    <div className='LoginPageTop'>
      <div className="container LoginPageContainer">

        {/* <img className='LoginPageImage' src="https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="" /> */}
        <img className='LoginPageImage' src="https://img.freepik.com/premium-vector/illustration-vector-graphic-cartoon-character-login_516790-1261.jpg?w=2000" alt="" />

      <form className='LoginForm border'>
      <h2 className='pb-3 text-center'>Login</h2>
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder='Email'></input>
    <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder='Password'></input>
  </div>
  {/* <div class="mb-3 form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1"></input>
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div> */}
  <div className='d-flex flex-column justify-content-between buttonsDiv'>
  <button type="button" class="btn loginButton">Login</button>
  <p className='text-center'>Not a member?<span onClick={(e)=>{
            navigate('/registration');
          }} className='subtextLoginButton'>Sign Up</span></p>
  </div>


</form>
      </div>
    </div>
  )
}
