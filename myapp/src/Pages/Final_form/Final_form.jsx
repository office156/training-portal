import React, { useState , useRef} from 'react'
import { ToastContainer, toast } from 'react-toastify';
import "./Final_form.css"
import { useNavigate } from 'react-router-dom'
import axios from 'axios'
import 'react-toastify/dist/ReactToastify.css';

export default function Final_form() {

    const navigate = useNavigate();
    const [name , setName] = useState("");
    const [institute , setInstitue] = useState("");
    
    const [email , setemail] = useState("");
    const [hodemail , sethodemail] = useState("");
    const [password , setpassword] = useState("");
    const [phone , setphone] = useState("");
    const [eduqua , seteduqua] = useState("option3");
    const [domain , setdomain] = useState("option1");
    const [address , setaddress] = useState("");
    // const [photo , setphoto] = useState();
    // const [aadharcard , setaadharcard] = useState();
    // const [option , setOption] = useState();
    const [file, setFile] = useState(null);
    const inputRef = useRef(null);
  
    const handleDrop = (e) => {
      e.preventDefault();
      const newFile = e.dataTransfer.files[0];
      setFile(newFile);
    };
  
    const handleDragOver = (e) => {
      e.preventDefault();
    };
  
    const handleFileInputChange = (event) => {
      const file = event.target.files[0];
      console.log("in change function")
      console.log(file.name)
      setFile(file);
    };
  
    const handleButtonClick = () => {
      inputRef.current.click();
    };




    // Adhaar upload area 

    const [adhaar, setadhaar] = useState(null);
    const inputRef1 = useRef(null);
  
    const handleDrop1 = (e) => {
      e.preventDefault();
      const newFile = e.dataTransfer.files[0];
      setadhaar(newFile);
    };
  
    const handleDragOver1 = (e) => {
      e.preventDefault();
    };
  
    const handleFileInputChange1 = (event) => {
      const file = event.target.files[0];
      console.log("in change function")
      console.log(file.name)
      setadhaar(file);
    };
  
    const handleButtonClick1 = () => {
      inputRef1.current.click();
    };

    // adhaar end 



    function selectOption(option) {
        const element = document.getElementById('dropdownMenuButton');
if (element) {
  element.innerHTML = option;
}
      }

    function selectOption1(option) {
        const element = document.getElementById('dropdownMenuButton1');
if (element) {
  element.innerHTML = option;
}

      }
      

    const handleSubmit = (event) => {
      // name , setName] = useState();
      // const [institute , setInstitue] = useState();
      
      // const [email , setemail] = useState();
      // const [hodemail , sethodemail] = useState();
      // const [password , setpassword] = useState();
      // const [phone , setphone] = useState();
      // const [eduqua , seteduqua] = useState("option3");
      // const [domain , setdomain] = useState("option1");
      // const [address
        event.preventDefault();
        if (name === "" || email === "" || institute === "" || hodemail === "" || phone === "" || address === "" || file === null || adhaar === null ) {
          toast.error("Please enter all the details")
        } else {
          const data = { "name" : name, "email" : email , "institute":institute , "address":address , "phone":phone , "password":password , "hodemail":hodemail , "eduqua":eduqua , "domain":domain , "photo":file , "aadharcard":adhaar};
        
        axios.post('http://localhost:8000/api/register', data ,  {
            headers: {
              'Content-Type': 'multipart/form-data'
            }
          }).then((response) => {
            console.log(response)
            setName('');
            setemail('');
            setInstitue('');
            setaddress('');
            setphone('');
            setpassword('');
            sethodemail('');
            seteduqua('');
            setdomain('');
            navigate("/done")

          })
          .catch((error)=>{
            // console.log(error)
            // console.log(error.response.data)
            // alert()
            toast.error(error.response.data)

          });
        }
        // if (email === "") {
        //   alert('Please enter your email');
        // }
        
        // Reset form inputs




      }



    // const [name , setName] = useState();
    return (
        <div className='container finalFormTop'>

            <h2 className='text-center formh1'>Registration</h2>
            <p className='text-center secondary-heading'>Kindly register on the portal to access course contents and learning materials</p>
            <form action="">
                <div className="row d-flex justify-content-between">
                    <div className="col-5">
                        <div class="flex-column py-2 d-flex"> <label class="form-control-label form-control-bold py-1">Name<span class="text-danger"> *</span></label> <input className='form-control' type="text" id="fname" name='fname' placeholder="Enter your name" onChange={e => setName(e.target.value)} required></input> </div>
                        <div class="flex-column py-2 d-flex"> <label class="form-control-label form-control-bold py-1">Email<span class="text-danger"> *</span></label> <input className='form-control' type="email"  placeholder="Enter your email" onChange={e => setemail(e.target.value)} required></input> </div>
                        <div class="flex-column py-2 d-flex"> <label class="form-control-label form-control-bold py-1">Password<span class="text-danger"> *</span></label> <input className='form-control' type="password"  placeholder="Create a password" onChange={e => setpassword(e.target.value)} required></input> </div>
                        <div class="flex-column py-2 d-flex"> <label class="form-control-label form-control-bold py-1">Educational Qualification<span class="text-danger"> *</span></label>
                            {/* <div class="dropdown" onChange={e => seteduqua(e.target.value)}>
                                <button id='dropdownMenuButton' class="w-100  dropdown_buttons registrationDropdownButtons p-2 text-start border dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Educational Qualification
                                </button>
                                <ul class="dropdown-menu">
                                    <li onClick={selectOption('Action')} className='dropdown-item' value="Action">Action</li>
                                    <li onClick={selectOption('Another Action')} className='dropdown-item' value="Another Action">Another action</li>
                                    <li onClick={selectOption('Some Action')} className='dropdown-item' value="Some Action">Something else here</li>
                                </ul>
                            </div> */}

<select defaultValue="option1" className='registrationDropdownButtons border p-2' id="myDropdown1" onChange={e => seteduqua(e.target.value)}>
  <option className='' value="option1">Option 1</option>
  <option className='' value="option2">Option 2</option>
  <option className='' value="option3">Option 3</option>
</select>

                        </div>

                    </div>
                    <div className="col-5">
                        <div class="flex-column py-2 d-flex"> <label class="form-control-label form-control-bold py-1">Institute<span class="text-danger"> *</span></label> <input className='form-control' type="text"  placeholder="Enter your institute name" onChange={e => setInstitue(e.target.value)} required></input> </div>
                        <div class="flex-column py-2 d-flex"> <label class="form-control-label form-control-bold py-1">HOD Email<span class="text-danger"> *</span></label> <input className='form-control' type="text"  placeholder="Enter your HOD's email" onChange={e => sethodemail(e.target.value)} required></input> </div>
                        <div class="flex-column py-2 d-flex"> <label class="form-control-label form-control-bold py-1">Phone number<span class="text-danger"> *</span></label> <input className='form-control' type="number"  placeholder="Enter your phone number" onChange={e => setphone(e.target.value)} required></input> </div>
                        <div class="flex-column py-2 d-flex"> <label class="form-control-label form-control-bold py-1">Domain<span class="text-danger"> *</span></label>
                            {/* <div class="dropdown" onChange={e => setdomain(e.target.value)}>
                                <button id='dropdownMenuButton1' class="dropdown_buttons registrationDropdownButtons p-2 text-start w-100 text-left border dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Domain
                                </button>
                                <ul class="dropdown-menu">
                                    <li onClick={selectOption1('Action')} value="Action"><p class="dropdown-item">Action</p></li>
                                    <li onClick={selectOption1('Another Action')} value="Another Action"><p class="dropdown-item">Another action</p></li>
                                    <li onClick={selectOption1('ome Action')} value="Some Action"><p class="dropdown-item">Something else here</p></li>
                                </ul>
                            </div> */}

<select defaultValue="option1" className='registrationDropdownButtons border p-2' id="myDropdown" onChange={e => setdomain(e.target.value)}>
  <option className='registrationDropdownButtons p-2' value="option1">Option 1</option>
  <option className='registrationDropdownButtons p-2' value="option2">Option 2</option>
  <option className='registrationDropdownButtons p-2' value="option3">Option 3</option>
</select>
                        </div>
                    </div>

                </div>

                <div className="row">
                <label class="form-control-label form-control-bold py-1">Address<span class="text-danger"> *</span></label>
                    <div class="form-floating">
                        <textarea class="form-control"  placeholder="Describe yourself here..." id="floatingTextarea" onChange={e => setaddress(e.target.value)} required></textarea>
                        <label className='floatingTextarea' for="floatingTextarea">Address</label>
                        {/* <textarea class="form-control" placeholder="Describe yourself here..."></textarea>
                        input:textarea */}
                    </div>
                </div>

                {/* <div className="row">
                    <DragDropFiles></DragDropFiles>
                </div> */}

                {/* <div className="row py-5">
                <div class="input-group mb-3">
  <label class="input-group-text" for="inputGroupFile01">Upload</label>
  <input type="file" class="form-control" id="inputGroupFile01"></input>
</div>
                </div>

                <div className="row pb-5">
                <div class="input-group mb-3">
  
  <input type="file" class="form-control" id="inputGroupFile02"></input>
  <label class="input-group-text" for="inputGroupFile02">Upload</label>
</div>
                </div> */}


{/* Drag drop files code */}

<div className="row py-2">
<label class="form-control-label form-control-bold py-1">Photo upload<span class="text-danger"> *</span></label>
<div>
      <div
      className="d-flex align-items-center justify-content-center"
        onDrop={handleDrop}
        onDragOver={handleDragOver}
        style={{ border: "1px solid #ced4da", height: "150px", borderRadius: "0.375rem" }}
      >
        <input 
        className="inputWithoutBorder"
            type="file"
            ref={inputRef}
            onChange={handleFileInputChange}
            hidden
            accept="image/png, image/jpeg"
            required
          />


          <div className="text-center">
          <button type="button" className="stop-transition" onClick={handleButtonClick}>
          {/* <FontAwesomeIcon icon="fa-regular fa-cloud-arrow-up" /> */}
          <i class="fa-solid fa-cloud-arrow-up"></i>
          </button>
          <p className="change-color"><span className="make-bold">Click to upload</span> or drag and drop <br/>
          PNG,JPG (max 5 MB)</p>
          </div>

      </div>
      {file && (
        <div>
          <p>File name: {file.name}</p>
          <p>File type: {file.type}</p>
          <p>File size: {file.size} bytes</p>
        </div>
      )}
    </div>
</div>

{/* Drag drop files code end*/}




{/* Drag drop files code */}

<div className="row pb-5">
<label class="form-control-label form-control-bold py-1">Adhaar card upload<span class="text-danger"> *</span></label>
<div>
      <div
      className="d-flex align-items-center justify-content-center"
        onDrop={handleDrop1}
        onDragOver={handleDragOver1}
        style={{ border: "1px solid #ced4da", height: "150px", borderRadius: "0.375rem" }}
      >
        <input 
        className="inputWithoutBorder"
            type="file"
            ref={inputRef1}
            onChange={handleFileInputChange1}
            hidden
            accept="image/png, image/jpeg"
            
          />


          <div className="text-center">
          <button type="button" className="stop-transition" onClick={handleButtonClick1}>
          {/* <FontAwesomeIcon icon="fa-regular fa-cloud-arrow-up" /> */}
          <i class="fa-solid fa-cloud-arrow-up"></i>
          </button>
          <p className="change-color"><span className="make-bold">Click to upload</span> or drag and drop <br/>
          PNG,JPG (max 5 MB)</p>
          </div>

      </div>
      {adhaar && (
        <div>
          <p>File name: {adhaar.name}</p>
          <p>File type: {adhaar.type}</p>
          <p>File size: {adhaar.size} bytes</p>
        </div>
      )}
    </div>
</div>

{/* Drag drop files code end*/}

                <div className="row d-flex justify-content-between">
                    <div className="col-4">
                        <a onClick={(e)=>{
                            navigate("/bulk-registration")
                        }} className='bulkRegistration' href="#">Click here for bulk registration</a>
                    </div>
                    <div className="col-4 text-center">
                        <button className='p-1 btn-register w-100' onClick={handleSubmit}>Register</button>
                        {/* <br /> */}
                        {/* <br /> */}
                        <a className='existingAccount' href="#">Already have an account?</a>
                    </div>
                </div>

            </form>

            <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>
        </div>

    )
}
