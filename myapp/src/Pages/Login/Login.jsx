import React, { useState } from 'react'
import "./Login.css"
import { useNavigate } from 'react-router-dom';
import Navbar from '../../Components/Navbar/Navbar';
import Footer from '../../Components/Footer/Footer';
import axios from 'axios';

export default function Login() {

  const [email , setEmail] = useState();
  const [password , setPassword] = useState();
  const navigate = useNavigate();
  
  const handleSubmit = (event) => {
    event.preventDefault();
    const data = {
      'email' : email,
      'password' : password
    }
    
    axios.post('http://localhost:8000/api/login', data).then((response) => {
            console.log(response)
            navigate("/my-courses")
          })
          .catch(error => console.log(error));
  }
  
  

  return (
    <div className='LoginPageTop'>
      <Navbar></Navbar>
      <h1 className='text-center LoginPageHeader'>Login</h1>
      <p className='LoginPageTopSubtext text-center'>Kindly login on the portal to access course contents and learning materials</p>
      <div className="container LoginPageContainer">
      
<form className='LoginForm'>
      
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label loginPageLabels">Email</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder='Email' onChange={e => setEmail(e.target.value)} required></input>
    <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label loginPageLabels">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder='Password' onChange={e => setPassword(e.target.value)} required></input>
  </div>
  {/* <div class="mb-3 form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1"></input>
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div> */}
  <div className='d-flex flex-column justify-content-between buttonsDiv'>
  <button type="button" class="p-2 loginButton" onClick={handleSubmit}>Login</button>
  <p className='text-center'>Not a member?<span onClick={(e)=>{
            navigate('/registration');
          }} className='subtextLoginButton'>Sign Up</span></p>
  </div>


</form>
      </div>

      <Footer className = "LoginPageFooter"></Footer>
    </div>
  )
}
