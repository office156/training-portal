import React from 'react'
import DragDropFiles from '../components/DragDropFiles'
import "./BulkRegistration.css"

export default function BulkRegistration() {
  return (
    <div className='container BulkRegistrationTop'>
      <div className="BulkHeading text-center">
        <p className="bulkHeader">Bulk Registration</p>
        <p className="bulkSubtext">In case of bulk registration, kindly download the excel sheet below, fill the information <br />
and upload by clicking on the link below.</p>
      </div>




<div className='centerItems'>
      <div className="fileDownload d-flex justify-content-center">

      <i class="fa-regular fa-file iconAdjust"></i>
      <div className="fileDownloadBulk d-flex justify-content-between align-items-center">
        <p className="fileName">Bulk_registration.xls <br /><span className='fileSize'>200KB</span> </p>
        <i class="fa-solid fa-cloud-arrow-down downloadIconAdjust"></i>
      </div>

      </div>
      </div>




      <div className="bulkUpload">
        <p className="subtextUploadBulk">Upload filled excel sheet here</p>
        <DragDropFiles></DragDropFiles>
      </div>

      <div className="registrationSheet text-center">
        <button type='button' className='btn registrationSheetButton'>Send Registration Sheet</button>
      </div>

    </div>
  )
}
