import React from 'react';
import Cards from '../../Components/Cards/Cards';
import Cards2 from '../../Components/Cards2/Cards2';
import Faq from '../../Components/FAQ/Faq';
import Footer from '../../Components/Footer/Footer';
import ImageSlider from '../../Components/ImageSlider/ImageSlider';
import InstructorSlider from '../../Components/InstructorSlider/InstructorSlider';
import LPathHome from '../../Components/LPathHome/LPathHome';
import Navbar from '../../Components/Navbar/Navbar';

export default function Home() {
  return <div className='HomePageTop'>
      <Navbar></Navbar>
      <ImageSlider></ImageSlider>
      {/* <Cards></Cards> */}
      <Cards2></Cards2>
      <LPathHome></LPathHome>
      <InstructorSlider></InstructorSlider>
      <Faq></Faq>
      <Footer></Footer>
  </div>;
}
