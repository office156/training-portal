import React from 'react';
import "./Navbar.css"
import { Link ,useNavigate  } from 'react-router-dom';
// Put any other imports below so that CSS from your
// components takes precedence over default styles.

export default function Navbar() {

  const navigate = useNavigate();

  function handleClick() { 
    navigate('/registration');
  }

  return <div>
 {/* <!-- Navigation --> */}

<nav class="navbar navbar-expand-lg bg-light">
  <div class="container">
    <a class="navbar-brand" href="#">HPCTraining</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ms-auto">
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="#">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="#">About Us</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Courses</a>
        </li>
        <li class="nav-item learningPathLink">
          <a class="nav-link">Learning Path</a>
        </li>
        <li class="nav-item loginLink">
          <a onClick={(e)=>{
            e.preventDefault();
            navigate('/login');
          }} class="nav-link">Login</a>
        </li>
        <li class="nav-item">
          <button type="button"  onClick={handleClick} class="NavbarSignUpButton p-2">Sign up</button>
          {/* <Link to="/registration" ></Link> */}
        </li>

    </ul>
    </div>
  </div>
</nav>

  </div>;
}
