import React from "react";
import "./Footer.css";

export default function Footer() {
  return (
    <div>
      {/* <!-- Remove the container if you want to extend the Footer to full width. --> */}
      {/* <!-- Footer --> */}
      <footer class="text-center text-lg-start text-white">
        {/* <!-- Grid container --> */}
        <div class="container p-4 pb-0">
          {/* <!-- Section: Links --> */}
          <section class="">
            {/* <!--Grid row--> */}
            <div class="row">
              {/* <!-- Grid column --> */}
              <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
                <h4 class="text-uppercase mb-4 font-weight-bold">
                  HPC Training
                </h4>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing
                  elit.Lorem ipsum dolor sit amet, consectetur adipisicing
                  elit.
                </p>
              </div>
              {/* <!-- Grid column --> */}


              <hr class="w-100 clearfix d-md-none" />

              {/* <!-- Grid column --> */}
              <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
                <h6 class="text-uppercase mb-4 font-weight-bold">
                  Useful links
                </h6>
                <p>
                  <a class="text-white">Home</a>
                </p>
                <p>
                  <a class="text-white">About</a>
                </p>
                <p>
                  <a class="text-white">Courses</a>
                </p>
                <p>
                  <a class="text-white">Learning Path</a>
                </p>
                <p>
                  <a class="text-white">Contact us</a>
                </p>
              </div>

              {/* <!-- Grid column --> */}
              <hr class="w-100 clearfix d-md-none" />

              {/* <!-- Grid column --> */}
              <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
                <h6 class="text-uppercase mb-4 font-weight-bold">Contact</h6>
                <p>
                  <i class="fas fa-home mr-3"></i> Address Line 1
                </p>
                <p>
                  <i class="fas fa-envelope mr-3"></i> gmail@gmail.com
                </p>
                <p>
                  <i class="fas fa-phone mr-3"></i> + 01 234 567 89
                </p>
              </div>
              {/* <!-- Grid column --> */}
            </div>
            {/* <!--Grid row--> */}
          </section>
          {/* <!-- Section: Links --> */}

          {/* <hr class="my-3"></hr> */}
        </div>
        {/* <!-- Grid container --> */}
      </footer>
      {/* <!-- Footer --> */}
    </div>
  );
}
