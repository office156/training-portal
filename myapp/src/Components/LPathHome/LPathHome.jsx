import React from 'react';
import "./LPathHome.css"

export default function LPathHome() {
  return <div className='container'>
      <div className="home-lpath">
          <img className='lpath-bg-img' src={require("../../Images/Lpath.png")} alt="" />
          <div class="overlay">
              <h5 className='lPathHeader'>Not sure where to <br></br> start learning ?</h5>
              <p className='lPathPar'>Learning paths will help you decide which course to enroll first</p>
              <button type="button" class="btn btn-light overlay-button"><p className='text-overlay'>See our learning paths    </p>   <i class="fa-sharp fa-solid fa-greater-than"></i></button>
          </div>
      </div>
  </div>;
}
