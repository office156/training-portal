import React from 'react';
import "./InstructorSlider.css"

export default function InstructorSlider() {
  return <div className='container'>
      <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
      <h3 className='instructorsHeader'>Instructors</h3>
  <div class="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
  </div>
  <div class="carousel-inner">
    <div class="carousel-item active">
      
      {/* <div class="carousel-caption carousel-caption-instructor  d-none d-md-block"> */}
      <div className="carousel-div">
      <img src={require('../../Images/Ellipse.png')} class="d-block carousel-img-instructor" alt="..."></img>
      <div className="carousel-inner-div">
      <h5>Himanshu Sharma</h5>
        <p>Information about instructor Information about instructor Information
about instructor Information about instructor</p>
      </div>

</div>
      {/* </div> */}
    </div>
    <div class="carousel-item">
      
      {/* <div class="carousel-caption carousel-caption-instructor  d-none d-md-block"> */}
      <div className="carousel-div">
      <img src={require('../../Images/Ellipse.png')} class="d-block carousel-img-instructor" alt="..."></img>
      <div className="carousel-inner-div">
      <h5>Himanshu Sharma</h5>
        <p>Information about instructor Information about instructor Information
about instructor Information about instructor</p>
      </div>

</div>
      {/* </div> */}
    </div>
    <div class="carousel-item">
      
      {/* <div class="carousel-caption carousel-caption-instructor  d-none d-md-block"> */}
      <div className="carousel-div">
      <img src={require('../../Images/Ellipse.png')} class="d-block carousel-img-instructor" alt="..."></img>
      <div className="carousel-inner-div">
      <h5>Himanshu Sharma</h5>
        <p>Information about instructor Information about instructor Information
about instructor Information about instructor</p>
      </div>

</div>
      {/* </div> */}
    </div>
    {/* <div class="carousel-item">
      <img src={require('../../Images/ken.jpg')} class="d-block carousel-img-instructor" alt="..."></img>
      <div class="carousel-caption carousel-caption-instructor d-none d-md-block">
        <h5>Second slide label</h5>
        <p>Some representative placeholder content for the second slide.</p>
      </div>
    </div>
    <div class="carousel-item">
      <img src={require('../../Images/pietro.jpg')} class="d-block carousel-img-instructor" alt="..."></img>
      <div class="carousel-caption carousel-caption-instructor d-none d-md-block">
        <h5>Third slide label</h5>
        <p>Some representative placeholder content for the third slide.</p>
      </div>
    </div> */}
  </div>
  {/* <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button> */}
</div>
  </div>;
}
