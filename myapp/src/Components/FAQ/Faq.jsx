import React from 'react'
import "./Faq.css"

export default function Faq() {
  return (
    <div className="container FaqPageTop">
        <h2 className='FAQheading mb-20'>Frequently Asked Questions</h2>
        <div className="container questionTop">
            <p className="faqQuestions pb-3">What are the benefits for enrolling in the courses?</p>
            <p className="faqQuestions pb-3">Who is eligible for enrolling in the courses?</p>
            <p className="faqQuestions pb-3">Can I enroll to more than one courses at a time?</p>
            <p className="faqQuestions pb-3">What all mode of payments are available on the platform?</p>
            <p className="faqQuestions pb-3">Whether course completion certificate will be issued?</p>

        </div>

        <div className="contactFaq d-flex justify-content-between align-items-center">
            <div className=''>
            <p className="contactFaqTitle">Still have any questions?</p>
            <p className='contactSubtext'>Can’t find the answer you’re looking for? Please contact us by click the button </p>
            </div>
            <button className='btn getInTouch' type='button'>Get in touch</button>
        </div>
    </div>
  )
}
