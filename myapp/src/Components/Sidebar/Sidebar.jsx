import React from 'react'
import "./Sidebar.css"
import { useNavigate } from 'react-router-dom'

export default function Sidebar() {


    const navigate = useNavigate()

  return (
    <div className='SideBarTop'>
        <div className="AdminSidebarHeader text-center">Dashboard</div>
        <div className="sidebarListItems d-flex flex-column justify-content-between align-items-between">
        <div className="listItemsTopSidebar">
            <ul className='p-0'>
                <li className='sideBarTopListElements py-2' onClick={()=>{
                    navigate("/admin")
                }}>
                <i class="fa-solid px-2 sidebarTopIcons fa-house"></i>
                <p className='sidebarlistItemNames p-0 m-0'>Home</p>
                </li>
                <li className='sideBarTopListElements py-2' onClick={()=>{
                    navigate("/admin-users")
                }}>
                <i class="fa-solid px-2 sidebarTopIcons fa-users"></i>
                <p className='sidebarlistItemNames p-0 m-0'>Users</p>
                </li>
                <li className='sideBarTopListElements py-2'>
                <i class="fa-solid px-2 sidebarTopIcons fa-layer-group"></i>
                <p className='sidebarlistItemNames p-0 m-0'>Courses</p>
                </li>
                <li className='sideBarTopListElements py-2'>
                <i class="fa-solid px-2 sidebarTopIcons fa-signal"></i>
                <p className='sidebarlistItemNames p-0 m-0'>Statistics</p>
                </li>
            </ul>
        </div>
        <div className='listItemsBottomSidebar'>
        <ul className='p-0'>
                <li className='sideBarTopListElements py-2'>
                <i class="fa-solid px-2 fa-life-ring"></i>
                <p className='sidebarlistItemNames p-0 m-0'>Support</p>
                </li>
                <li className='sideBarTopListElements py-2'>
                <i class="fa-solid px-2 fa-gear"></i>
                <p className='sidebarlistItemNames p-0 m-0'>Settings</p>
                </li>
            </ul>
            <hr className='mx-2' />
            <div className="adminProfileMini d-flex align-items-center">
                <img src="https://i0.wp.com/newdoorfiji.com/wp-content/uploads/2018/03/profile-img-1.jpg?ssl=1" alt="" className="profilePhotoSidebar me-2" />
                <div className="text">
                    <h4 className='sidebarAdminHeading'>Admin</h4>
                    <p className="p-0 m-0 adminEmail">admin@hpctraining.com</p>
                </div>
                <i class="ps-1 fa-solid fa-right-from-bracket"></i>
            </div>
        </div>
        </div>
    </div>
  )
}
