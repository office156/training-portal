import './App.css';
import Navbar from './Components/Navbar/Navbar';
import Topbar from './Components/Topbar/Topbar';
import BulkRegistration from './Pages/BulkRegistration/BulkRegistration';
import FileUpload from './Pages/File_upload/File_upload';
import Final_form from './Pages/Final_form/Final_form';
import Home from './Pages/Home/Home';
import { BrowserRouter as Router, Switch, Route, Routes } from 'react-router-dom';
import Login from './Pages/Login/Login';
import RegDone from './Pages/RegsiatrationDone/RegDone';
import AdminPanel from './Pages/AdminPanel/AdminPanel';
import MyCourses from './Pages/MyCourses/MyCourses';
import UserList from './Components/UserList/UserList';

function App() {
  return (

    <Router>
    <div className="App">
      <Routes>
                 <Route exact path='/' element={< Home />}></Route>
                 <Route exact path="/registration" element={<><Navbar/><Final_form/></>}/>
                 <Route exact path="/bulk-registration" element={<><Navbar/><BulkRegistration/></>}/>
                 <Route exact path="/login" element={<Login/>}/>
                 <Route exact path="/done" element={<RegDone/>}/>
                 <Route exact path="/admin" element={<AdminPanel/>}/>
                 <Route exact path="/admin-users" element={<UserList/>}/>
                 <Route exact path="/my-courses" element={<MyCourses/>}/>
          </Routes>
    </div>
    </Router>
    
  );
}

export default App;
